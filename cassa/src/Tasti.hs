-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE NoMonomorphismRestriction  #-}
{-# LANGUAGE OverloadedLists            #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE RecursiveDo                #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

import           Control.Lens
import           Control.Monad
import qualified Data.Dependent.Map          as DM
import           Data.GADT.Compare.TH
import           Data.GADT.Compare
import           Data.List
import qualified Data.Map                    as M
import           Data.Maybe
import           Data.Monoid                 ((<>))
import           Data.Text                   (Text, pack, strip, unpack)
import           Data.Time
import           GHCJS.DOM
import           GHCJS.DOM.Window            (getInnerWidth)
import           Language.Javascript.JSaddle (MonadJSM, liftJSM)
import           Lib
import           Reflex.Dom
import Data.Bifunctor
import Text.CSV
import Data.Text.Encoding
import Crypto.PasswordStore


--  library -------------
minzero x y | x + y >= 0 = x + y
            | True = 0

ite x _ True = x
ite _ y _    = y
---------------------

-- | sample internal width
iw
    :: (PerformEvent t m, MonadJSM (Performable m))
    => Event t b
    -> m (Event t Int)
iw x = performEvent ((liftJSM currentWindowUnchecked >>= getInnerWidth) <$ x)

-- | button
button0 :: MS m => Text -> m a -> m (ES ())
button0 c content = do
    (e, _) <- elClass' "button" c content
    return $ domEvent Click e


-- | accepting widget
conferma :: MS m => Text -> m (ES Bool)
conferma t = divClass "conferma" $ el "ul" $ do
    el "li" $ divClass "reason" $ text t
    el "li" $ elClass "ul" "conferma_buttons" $ do
        n <- el "li" $ button0 "" $ icon ["times"]
        y <- el "li" $ button0 "" $ icon ["check"]
        return $ leftmost [False <$ n, True <$ y]

showPoints = pack . (++ "p") . show . (\(Point p) -> p )

data Product = Product {
    image :: Text,
    value :: Points,
    name  :: Text
} deriving (Eq,Ord, Show)

data Bill = Bill
          { description :: [(Text,Int)]
          , amount      :: Points
          }
-- | product key
prodotto
    :: MS m
    => DS Bill
    -> DS Int -- delta (pos neg)
    -> DS Int -- size
    -> Product
    -> DS Int  -- count
    -> m (ES Int) -- delta event
prodotto bill opb dz (Product i n t) dc = el "li" $ do
    let size x =
            [ ("src"   ,i)
            , ("height", pack $ show x)
            , ("width" , pack $ show x)
            ]
        content = do
            elDynAttr "img" (size <$> dz) $ return ()
            divClass "testuale" $ text (t <> "-" <> showPoints n)
            dyn $ elClass "div" "count" . text . pack . show <$> dc
    e <- button0 "btn btn-primary" $ content
    let check d (Bill _ w) = w - fromIntegral d * n >= 0
    return $ current opb <@ gate (check <$> current opb  <*> current bill) e

type Cart = M.Map Product Int
data User = User
            { userName  :: Text
            , leftOvers :: Points
            , cardNo    :: Card
            }
newtype Card = Card Int deriving (Eq,Ord, Show, Num)

showCard (Card u) = pack . show $ u
data Store = Store
           { storeName :: Text
           , storeProducts :: Text
           , storeUsers :: Text
           , storePassword :: Text
           }
newtype Points = Point Float deriving (Eq,Ord, Show,Num, Fractional)
---------- hard coding should go away ---------


selectUser :: [User] -> Card -> Maybe User
selectUser users c = find ((== c) . cardNo) $ users

emptyCart :: [Product] -> Cart
emptyCart products = M.fromList $ zip products $ repeat 0
------------------------------------------------

control
    :: MS m
    => Int -- actual number of keys per row
    -> m (DS Int, ES (), DS Int, DS Int, ES (), ES ())
control productRow
    = elClass "ul" "control" $ do
        larger <- fmap ((+1) <$)
                . el "li"
                $ button0  "" $ icon ["search-minus"] -- (text "<->")
        smaller <- fmap (subtract 1 <$)
                . el "li"
                $ button0 "" $ icon ["search-plus"] -- (text ">-<")
        let chsz = leftmost [larger, smaller]
        sz <- foldDyn (\f x -> max 1 (f x)) productRow $ chsz
        r <- getPostBuild
        w <- iw $ leftmost [() <$ chsz, r]
        let f n w = (w - 45 - (n + 1) * 2) `div` n
        sz1 <- holdDyn 80 $ attachWith f (current sz) w
        reset <- el "li" $ button0 "" $ icon ["trash"] -- (text "RESET")
        user <- el "li" .  button0 "" $ icon ["user"] -- text "UTENTE"
        rec e  <- el "li"
                    . button0 ""
                    . dyn
                    . (<$> opb)
                    . fmap (icon . return)
                    $ ite "minus" "plus"
            opb <- foldDyn ($) True $
                leftmost    [   const not <$> e
                            ,   const True <$ reset
                            ]
        commit <- el "li" .  button0 "" $ icon ["check"] -- text "UTENTE"
        return (ite 1 (-1) <$> opb, reset, sz1,sz, user, commit)

data Ctx = Ctx {
    _cart :: Cart,
    _user :: User
    }

makeLenses ''Ctx

emptyCtx :: [Product] -> User -> Ctx
emptyCtx products u = Ctx (emptyCart products) u

type CtxMap = M.Map Card Ctx


data State = State {
    _productRow   :: Int,
    _selectedUser :: Maybe Card,
    _carts        :: CtxMap
    }

makeLenses ''State

data View m s
    = MakeCart s
    | DefineUsers s
    | Modal (m ()) Text (View m s, View m s)
    | ModalLeave (m ()) Text (View m s)

data Desking m a where
    Leave :: Desking m ()
    Desking :: Desking m (View m State)

instance GEq (Desking m) where
  Leave `geq` Leave= Just Refl
  Desking `geq` Desking = Just Refl
  _ `geq` _ = Nothing

instance GCompare (Desking m) where
  Leave `gcompare` Desking = GLT
  Desking `gcompare` Leave = GGT
  Leave `gcompare` Leave = GEQ
  Desking `gcompare` Desking = GEQ

getNewUser us = divClass "new_user" $ do
    let tryRead s = case reads . unpack . strip $ s of
                     [(u,"")] -> Just $ Card u
                     _        -> Nothing
    rec     t <- divClass "enter_card"
                $ textInput $
                    def
                    & textInputConfig_setValue .~ e
                    & textInputConfig_attributes
                                .~ constDyn [("placeholder", "ingresso tessera")]

            let e = "" <$ keypress Enter t
                g :: DS Bool
                g = (/= "") <$> t ^. textInput_value
            divClass "report_card" $ dyn $ flip fmap ((,,) <$> r <*> s <*> g) $ \(r,s,g) ->
                case g of
                    False -> return ()
                    True -> case r of
                        Nothing -> elClass "span" "noparse" $ text "numero tessera non valido"
                        Just t -> case s of
                            Nothing -> elClass "span" "nocard" $ text ("tessera inesistente: " <> showCard t)
                            Just u -> elClass "span" "user" $ text (userName u <> ", " <> showPoints (leftOvers u))
            r <- holdDyn Nothing $ tryRead <$> _textInput_input t
            s <- holdDyn Nothing $ attachWith (\us r -> r >>= selectUser us) (current us) (updated r)
    return $ fmapMaybe id $ tag (current s) e

selectCtx servedCard ctxs
        = maybe (error "Unrelated card") id $ M.lookup servedCard ctxs

showBill :: MS m => Ctx -> m ()
showBill ctx = do
        divClass "title" $ text "Scontrino"
        let Bill is w = bill ctx
        elClass "table" "bill" $ do
            el "tr" $ do
                el "th" $ text "articolo"
                el "th" $ text "confezioni"
            forM_ is $ \(p,q) ->
                el "tr" $ do
                    elClass "td" "product" $ text p
                    elClass "td" "quantity" $ text $ pack . show $ q

bill :: Ctx -> Bill
bill (Ctx c (User _ w _)) =
        let f (Product _ p name, t) b@(Bill ps w)
                |  t > 0 = Bill ((name, t) : ps) (w - fromIntegral t * p)
                | otherwise = b
        in foldr f (Bill [] w) $ M.assocs c


data Defining a where
    NewUser :: Defining User
    DeleteUser :: Defining Card
    EnterUser :: Defining Card

deriveGEq ''Defining
deriveGCompare ''Defining


application :: MS m => Text -> Text -> View m State -> m (Cable (Desking m))


application _ _ (MakeCart (State productRow Nothing ctxs)) = error "accessing a cart of noone"
application bs _ (MakeCart (State productRow (Just servedCard) ctxs))
    = do
        let ctx = selectCtx servedCard ctxs
        rec divClass "title"
                $ dynText
                $ (\b -> "Spesa di "
                    <> showCard servedCard
                    <> " ("
                    <> showPoints (amount b)
                    <> " )"
                  ) <$> billD
            let billD = bill <$> d
            (opb, reset, sz, prD, bu, commit) <- control productRow
            d :: DS Ctx <- elClass "ul" "tasti" $ do
                    rec     mc <- listViewWithKey (view cart <$> d) (prodotto billD opb sz)
                            d <- foldDyn ($) ctx
                                $  (over cart . M.unionWith minzero) <$> mc
                    return d

        let setCtx :: Functor f => f Ctx -> f CtxMap
            setCtx
                = fmap
                $ \d ->  M.insert servedCard d ctxs
            rmCtx :: CtxMap
            rmCtx = M.delete servedCard ctxs
            resetting
                = Modal (return ()) 
                        ("Azzerare la spesa di " <> showCard servedCard <> " ?") 
                <$> attach
                    ( MakeCart 
                        <$> ( State 
                            <$> current prD 
                            <*> pure (Just servedCard) 
                            <*> setCtx (current d)
                            )
                    )
                    ( MakeCart 
                        <$> attachWith 
                            (\d e -> State d (Just servedCard) e)
                            (current prD)
                            (setCtx $ over cart (fmap (const 0)) ctx <$ reset)
                    )
            committing
                = attachWith
                    (\s1 s2  -> Modal (domMorph (\x -> showBill x >> return never) d >> return ()) 
                        ("Concludere la spesa di " <> showCard servedCard <> " ?") 
                        (MakeCart s1, DefineUsers s2)
                    )
                    (State <$> current prD <*> pure (Just servedCard) <*> setCtx (current d))
                    $ attachWith 
                        (\d e -> State d (Just servedCard) e)
                        (current prD)
                        (rmCtx <$ commit)
            userview 
                = DefineUsers 
                <$> tag 
                    (State <$> current prD <*> pure Nothing <*> setCtx (current d)) 
                    bu
        return $ wire' Desking $ leftmost [resetting, userview, committing]


application _ _ (Modal s q (sn, sy)) = do
    s
    yn <- conferma q
    return $ wire' Desking $ leftmost [sy <$ ffilter id yn, sn <$ ffilter not yn]

application _ _ (ModalLeave s q sn) = do
    s
    yn <- conferma q
    return $ mergeDSums [Leave DM.:=> () <$ ffilter id yn, Desking DM.:=>  sn <$ ffilter not yn]

application bs us (DefineUsers (State productRow servedCard ctxs)) = do

    pb <- getPostBuild 
    bs <- fmap (map csvProduct) <$> mungeData pb bs "elenco prodotti"
    us <- fmap (map csvUser) <$> mungeData pb us "elenco utenti"
    logout <- divClass "title" $ do
        text "Utenti serviti"
        elClass "span" "logout" $ button0 "" $ icon ["sign-out"]
    rec ne <-  getNewUser us
        ope <- fmap (fmap (snd . head . M.assocs))
                . elClass "ul" "users" $ listViewWithKey ctxs' $ \u _ -> do
                    el "li" $ do
                        elClass  "ul" "users_actions" $ do
                                d <- elClass "li" "delete_user" $ button0 "" $ icon ["trash"]
                                e <- elClass "li" "enter_user" $ button0 "" $ text (showCard u)
                                return $ mergeDSums [ DeleteUser DM.:=> u <$ d
                                                    , EnterUser DM.:=> u <$ e
                                                    ]
        let g :: [Product] -> User -> CtxMap -> CtxMap
            g bs u@(User _ _ c)  = M.insert c $ emptyCtx bs u
        ctxs'' <- foldDyn ($) mempty $ attachWith g (current bs) ne
        let correct :: [Product] -> Ctx -> Ctx 
            correct bs (Ctx m u) = Ctx (foldr f mempty bs) u  where
                f k =  M.insert k (M.findWithDefault 0 k m)
            ctxs' = (\bs ctxs' -> fmap (correct bs) ctxs <> ctxs') <$> bs <*> ctxs''

    let switch ctxs' sc = MakeCart (State productRow (Just sc) ctxs')
        deleting = attachWith
            (\c1 (c2, c) -> 
                Modal (return ()) 
                    ("Cestinare la spesa di " <> showCard c <> " ?")
                    ( DefineUsers (State productRow servedCard c1)
                    , DefineUsers (State productRow Nothing c2))
            )
            (current ctxs')
            (attachWith (\ctxs c -> (M.delete c ctxs, c)) (current ctxs') $ pick DeleteUser ope)

    return $ wire' Desking $ leftmost
            [ attachWith switch (current ctxs') (pick EnterUser ope)
            , deleting
            , tag ( ModalLeave (return ()) "Uscire dall'emporio ?" 
                    . DefineUsers 
                    . State productRow servedCard 
                    <$> current ctxs'
                  ) logout
            ]

initialView :: View m State
initialView = DefineUsers initialState

initialState = State 3 Nothing mempty

data PreApplication = UnAuthorized | SelectRole Store | Authorized Store

preApplication :: MS m => ([Store], PreApplication) -> m (ES PreApplication)
preApplication (xs, UnAuthorized) = do
    divClass "title" $ text "Selezione emporio"
    let checkPassword (Left False) = 
            fmap leftmost . elClass "ul" "stores" $ forM xs $ \x ->
                fmap (Right x <$) $ el "li" $ button0 "" $ text (storeName x)
        checkPassword (Right x@(Store _ _ _ p)) = do

            rec t <- divClass "enter_password"
                    $ textInput $
                    def
                    & textInputConfig_setValue .~ e
                    & textInputConfig_inputType .~ "password"
                    & textInputConfig_attributes
                                .~ constDyn [("placeholder", "password amministratore")]

                let e = "" <$ keypress Enter t
            let accept = attachWith verifyPassword
                            (encodeUtf8 <$> current (t ^. textInput_value))
                            $ encodeUtf8 p <$ keypress Enter t
            return $ Left <$> accept
        checkPassword _ = return never
    rec s <- holdDyn (Left False) c
        c <- domMorph checkPassword s
    return . fmap (\(Right x) -> Authorized x) $ tag (current s) $ ffilter id $ filterLeft c
preApplication (_, Authorized (Store _ bs us _) ) = do
    
    rec     viewChange <- domMorph (application bs us) viewState -- page controller
            -- state of the view
            viewState <- holdDyn initialView $ pick Desking viewChange
    return $ UnAuthorized <$ pick Leave viewChange


csvProduct :: [String] -> Product
csvProduct [n,u,p] = Product (pack u) (Point $ read p) (pack n)

csvStore :: [String] -> Store
csvStore [n,bs, us,p] = Store (pack n) (pack bs) (pack us) (pack p)

csvUser :: [String] -> User
csvUser [t,n,p] = User (pack n) (Point $ read p) (Card $ read t) 

mungeData :: MS m => ES a -> Text -> String -> m (DS [[String]])
mungeData e s sc = do
    let req = xhrRequest "GET" 
                (  "https://www.googleapis.com/drive/v3/files/" 
                <> s
                <> "/export?mimeType=text/csv"
                <> "&key=AIzaSyDrzx1uIGkJxduZeDi5pKmZ1Y16CEqTquE"
                )
                def
    asyncReq <- performRequestAsync (req <$ e)
    holdDyn []  $ fmap tail 
                $ filterRight
                $ fmapMaybe id
                $ fmap (  
                        first (pack . show)
                        . parseCSV sc
                        . unpack
                       )
                <$> _xhrResponse_responseText 
                <$> asyncReq

shopList="1w9SdzOahmoVBAjIC77yaDRVdxFi6GNHk1bOZp_Wo2vE"

main = mainWidget $ do
    viewport
    awesome
    pb <- getPostBuild
    es <- fmap (map csvStore) <$> mungeData pb shopList "elenco empori"
    css  -- insert css link

    rec     paE <- domMorph preApplication $ (,) <$> es <*> paD
            paD <- holdDyn UnAuthorized paE
    return ()

css =
    elAttr
            "link"
            [("rel", "stylesheet"), ("type", "text/css"), ("href", "cassa.css")]
        $ return ()

awesome =
    elAttr "script"
        [("src", "https://use.fontawesome.com/0b08d58b80.js")
        , ("type", "text/javascript")]
        $ return ()

viewport = elAttr "meta"
    [("name","viewport"),("content","width=device-width, user-scalable=no,initial-scale=1.0")]
    $ return ()

favicon = elAttr "link"
        [("rel","shortcut icon"),("type","image/png"),("href","favicon.png")]
        $ return ()
