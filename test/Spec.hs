import           Administration
import           Database.InMemory (adm, db)
import           Test.Tasty
import           Test.Tasty.Bdd

main :: IO ()
main = testGeneric db adm

testGeneric :: TestableMonad (m s) => Database m s -> Administration m s -> IO ()
testGeneric db adm
    = defaultMain
    $ testGroup "administration"
    $ [
        testBehavior "admit superuser"
        $ Given (cleanDB db)
        $ When (authorizeMaster adm (Email "super@dumb.net")  (Password "123"))
        $ Then (\(Just i) -> tokenOf adm i >>= \(Just e) -> e @?= (Email "super@dumb.net"))
        $ End
    ]
