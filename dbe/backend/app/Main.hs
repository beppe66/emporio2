{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE TypeOperators              #-}
{-# LANGUAGE ViewPatterns               #-}

import           Control.Lens                  hiding ((.=))
import           Control.Monad.Except
import           Control.Monad.Logger          (LoggingT, runStdoutLoggingT)
import           Control.Monad.Trans
import           Common.Data
import           Data.Aeson                    hiding (json)
import           Data.HVect                    hiding (pack)
import           Data.Monoid                   ((<>))
import           Data.Text                     (Text, pack, unpack)
import           Data.Text.Lazy                (toStrict)
import           Data.Time.Calendar
import           Data.Time.Clock
import           Data.Time.Lens
import           Database.Persist              hiding (get)
import qualified Database.Persist              as P
import           Database.Persist.Sqlite       hiding (get)
import           Database.Persist.TH
import           Network.Wai.Middleware.Static
import           System.Environment
import           System.Random
import           Text.Blaze.Html.Renderer.Text
import qualified Text.Blaze.Html5              as H
import           Web.Spock
import           Web.Spock.Config

import           Authenticate
import           Common.Communication
import           Lib
import           Mailing
import           Network.HTTP.Types
import           Roles

catchNothing e Nothing  = throwError e
catchNothing _ (Just x) = return x

printlog :: MonadIO m => ActionCtxT ctx m ctx
printlog = request >>= liftIO . print >> getContext

getBody :: (FromJSON a, MonadIO m)  => ExceptT Text (ActionCtxT ctx m) a
getBody = do
    mt <- lift $ jsonBody
    catchNothing "Failed to parse request body" mt

main :: IO ()
main = do
    pool     <- runStdoutLoggingT $ createSqlitePool "empori.db" 5
    spockCfg <- defaultSpockCfg Nothing (PCPool pool) ()
    runStdoutLoggingT $ runSqlPool (runMigration migrateAll) pool
    runSpock 8080 (spock spockCfg app)

login :: Api ctx
login = get ("login" <//> var) $ \lg -> do
    m <- runSQL $ selectList [AuthenticationCookie ==. lg] []
    case m of
        []           -> return ()
        [Entity _ x] -> writeSession (Just $ view authenticationCookie x)
        _            -> error "repeated cookie"
    h <- liftIO $ getEnv "hosting"
    redirect $ pack h <> "/index.html"

register :: Api ctx
register = do
    get ("register" <//> var) $ \x -> do
        registerA x
        html $ toStrict . renderHtml $ H.docTypeHtml $ do
            H.head $ do
                H.title "Registrazione"
                H.meta
                    H.! H.dataAttribute
                            "content"
                            "width=device-width, user-scalable=no,initial-scale=1.0"
                    H.! H.dataAttribute "name" "viewport"
            H.body $ do
                H.p "Una mail con il login è stata spedita."


logout = get "logout" $ writeSession Nothing
whoami :: ListContains n WhoAmI xs => Api (HVect xs)
whoami = get "whoami" $ do
    w :: WhoAmI <- findFirst <$> getContext
    json w

app :: Api ctx
app = prehook printlog $ prehook (return HNil) $ do
    middleware (staticPolicy (addBase "static"))
    login
    register
    prehook authH $ prehook whoamiH $ do
        logout
        whoami
        -- super
        get "jump" $ superH $ do
            allAuth <- runSQL $ selectList [] [Asc AuthenticationEmail]
            json allAuth
        get "stores" $ superH $ do
            allStores <- runSQL $ selectList [] [Asc StoreId]
            json allStores
        get ("stores" <//> "delete" <//> var) $ \storeId -> superH $ do
            ls <- runSQL $ deleteCascadeWhere [StoreId ==. toSqlKey storeId]
            json ()
        post "store" $ superH $ do
            maybeStore :: Maybe Store <- jsonBody
            case maybeStore of
                Nothing -> jsonError 1 "Failed to parse request body as Store"
                Just theStore -> do
                    newId <- runSQL $ insert theStore
                    json $ newId
        get ("store" <//> var) $ \k -> superH $ do
                r <- runSQL $ P.getJust (toSqlKey k :: Key Store)
                json r
        post ("store" <//> var <//> "name") $ \k -> superH $ do
            maybeName :: Maybe Text <- jsonBody
            case maybeName of
                Nothing -> jsonError 1 "Failed to parse request body as Text"
                Just t -> do
                    runSQL $ update k [Update StoreName t Assign]
                    json $ object ["result" .= String "success"]
        get ("admins" <//> var) $ \storeId -> superH $ do
            ls <- runSQL
                $ selectList [AdminStore ==. toSqlKey storeId] [Asc AdminId]
            json ls
        get ("admins" <//> "delete" <//> var) $ \adminId -> superH $ do
            ls <- runSQL $ deleteCascadeWhere [AdminId ==. toSqlKey adminId]
            json ()
        get ("admin" <//> var) $ \k -> superH $ do
                r <- runSQL $ P.getJust (toSqlKey k :: Key Admin)
                json r

        post ("admin" <//> var <//> "email") $ \k -> superH $ do
            maybeEmail :: Maybe Text <- jsonBody
            case maybeEmail of
                Nothing -> jsonError 1 "Failed to parse request body as Text"
                Just t -> do
                    runSQL $ update k [Update AdminEmail t Assign]
                    upsertAuthentication t
                    json $ object ["result" .= String "success"]
        post "admin" $ superH $ do
            maybeAdmin :: Maybe Admin <- jsonBody
            case maybeAdmin of
                Nothing -> jsonError 1 "Failed to parse request body as Admin"
                Just theAdmin -> do
                    newId <- runSQL $ insert theAdmin
                    upsertAuthentication $ view adminEmail theAdmin
                    json newId
        get ("products" <//> var <//> "selling") $ \(toSqlKey -> storeId)
            -> roleH [ROperator storeId] $ do
            ls <- runSQL
                $ selectList [ProductStore ==. storeId, ProductState ==. "A"] [Asc ProductId]
            json ls

        get ("products" <//> var) $ \(toSqlKey -> storeId)
            -> roleH [RAdmin storeId] $ do
            ls <- runSQL
                $ selectList [ProductStore ==. storeId] [Asc ProductId]
            json ls
        get ("product" <//> var <//> var)
                $ \(toSqlKey -> sk) k -> adminH sk $ do
                r <- runSQL $ P.getJust (toSqlKey k :: Key Product)
                json r
        get ("products" <//> var <//> "delete" <//> var)
                $ \(toSqlKey -> sk) productId -> adminH sk $ do
                    runSQL $ deleteCascadeWhere
                        [ProductId ==. toSqlKey productId
                        , ProductStore ==. sk
                        ]
                    json ()
        post ("product" <//> var <//> var <//> var)
            $ \(toSqlKey -> sk) (toSqlKey -> pk) l
            -> adminH sk
            $ do
                r <- runExceptT $ do
                    mp <- lift $ runSQL $ P.get pk
                    Product _ _ _ sk' _ <- catchNothing "No product on the key" mp
                    when (sk /= sk') $ throwError "Can't touch that product"
                    case (l :: Text) of
                        "name" -> do
                            t  <- getBody
                            lift . runSQL $ update pk [Update ProductName t Assign]
                        "points" -> do
                            t  <- getBody
                            lift $ runSQL $ update pk [Update ProductPoints t Assign]
                        "image" -> do
                            t  <- getBody
                            lift $ runSQL $ update pk [Update ProductImage t Assign]
                        "state" -> do
                            t  <- getBody
                            lift $ runSQL $ update pk [Update ProductState t Assign]
                case r of
                     Left e -> jsonError 1 e
                     _      -> json $ object ["result" .= String "success"]
        post ("product" <//> var) $ \storeId -> adminH storeId $ do
            maybeProduct :: Maybe Product <- jsonBody
            case maybeProduct of
                Nothing -> jsonError 1 "Failed to parse request body as Product"
                Just x ->
                    if x ^. productStore == storeId then
                        runSQL (insert x) >>= json
                    else jsonError 1 "Trying to register a product for a different store"

        get ("users" <//> var) $ \(toSqlKey -> sid)
            -> roleH [ROperator sid, RAdmin sid] $ do
            ls <- runSQL
                $ selectList [UserStore ==. sid] [Asc UserId]
            json ls
        get ("user" <//> var <//> var)
                $ \(toSqlKey -> sk) k -> adminH sk $ do
                r <- runSQL $ P.getJust (toSqlKey k :: Key User)
                json r
        get ("users" <//> var <//> "delete" <//> var)
                $ \(toSqlKey -> sk) userId -> adminH sk $ do
                    runSQL $ deleteCascadeWhere
                        [UserId ==. toSqlKey userId
                        , UserStore ==. sk
                        ]
                    json ()
        post ("user" <//> var <//> var <//> var)
            $ \(toSqlKey -> sk) (toSqlKey -> pk) l
            -> adminH sk
            $ do
                r <- runExceptT $ do
                    mp <- lift $ runSQL $ P.get pk
                    User _ _ _ _ sk' _ <- catchNothing "No user on the key" mp
                    when (sk /= sk') $ throwError "Can't touch that user"
                    case (l :: Text) of
                        "card" -> do
                            t  <- getBody
                            lift . runSQL $ update pk [Update UserCard t Assign]
                        "email" -> do
                            t  <- getBody
                            lift . runSQL $ update pk [Update UserEmail t Assign]
                            lift $ upsertAuthentication t
                        "points" -> do
                            t  <- getBody
                            lift $ runSQL $ update pk [Update UserPoints t Assign]
                        "nickname" -> do
                            t  <- getBody
                            lift $ runSQL $ update pk [Update UserNickname t Assign]
                        "state" -> do
                            t  <- getBody
                            lift $ runSQL $ update pk [Update UserState t Assign]
                case r of
                     Left e -> jsonError 1 e
                     _      -> json $ object ["result" .= String "success"]
        post ("user" <//> var) $ \storeId -> adminH storeId $ do
            maybeUser :: Maybe User <- jsonBody
            case maybeUser of
                Nothing -> jsonError 1 "Failed to parse request body as User"
                Just x ->
                    if x ^. userStore == storeId then do
                        runSQL (insert x) >>= json
                        upsertAuthentication $ x ^. userEmail
                    else jsonError 1 "Trying to register a user for a different store"

        get ("operators" <//> var) $ \(toSqlKey -> storeId) -> adminH storeId $ do
            ls <- runSQL
                $ selectList [OperatorStore ==. storeId] [Asc OperatorId]
            json ls
        get ("operator" <//> var <//> var)
                $ \(toSqlKey -> sk) k -> adminH sk $ do
                r <- runSQL $ P.getJust (toSqlKey k :: Key Operator)
                json r
        get ("operators" <//> var <//> "delete" <//> var)
                $ \(toSqlKey -> sk) operatorId -> adminH sk $ do
                    runSQL $ deleteCascadeWhere
                        [OperatorId ==. toSqlKey operatorId
                        , OperatorStore ==. sk
                        ]
                    json ()
        post ("operator" <//> var <//> var <//> var)
            $ \(toSqlKey -> sk) (toSqlKey -> pk) l
            -> adminH sk
            $ do
                r <- runExceptT $ do
                    mp <- lift $ runSQL $ P.get pk
                    Operator e  _ sk' _ <- catchNothing "No operator on the key" mp
                    when (sk /= sk') $ throwError "Can't touch that operator"
                    case (l :: Text) of
                        "email" -> do
                            t  <- getBody
                            lift . runSQL $ update pk [Update OperatorEmail t Assign]
                            lift $ upsertAuthentication t
                        "nickname" -> do
                            t  <- getBody
                            lift $ runSQL $ update pk [Update OperatorNickname t Assign]
                        "state" -> do
                            t  <- getBody
                            lift $ runSQL $ update pk [Update OperatorState t Assign]
                case r of
                     Left e -> jsonError 1 e
                     _      -> json $ object ["result" .= String "success"]
        post ("operator" <//> var) $ \storeId -> adminH storeId $ do
            maybeOperator :: Maybe Operator <- jsonBody
            case maybeOperator of
                Nothing -> jsonError 1 "Failed to parse request body as Operator"
                Just x ->
                    if x ^. operatorStore == storeId then do
                        newId <- runSQL $ insert x
                        upsertAuthentication $ x ^. operatorEmail
                        json newId
                    else jsonError 1 "Trying to register a operator for a different store"
        post ("shopping" <//> var) $ \storeId -> operatorH storeId $ do
            r <- runExceptT $ do
                ls :: [Shopping] <- getBody
                d <- liftIO $ utctDay <$> getCurrentTime
                lift $ runSQL $ insertMany (set (traverse . shoppingDate) d $ ls)
            case r of
                 Left e -> jsonError 1 e
                 _      -> json $ object ["result" .= String "success"]

        get ("shopping" <//> var <//> var) $ \sid uid -> operatorH sid $ do
            r <- runExceptT $ do
                d <- liftIO monthStart
                lift $ runSQL $ selectList [ShoppingUser ==. uid, ShoppingDate >=. d]
                        [Asc ShoppingDate]
            case r of
                 Left e  -> jsonError 1 e
                 Right r -> json r

monthStart = set days 1 <$> utctDay <$> getCurrentTime



{-post "users" $ do
        maybeUser <- jsonBody :: ApiAction (Maybe User)
        case maybeUser of
            Nothing -> jsonError 1 "Failed to parse request body as User"
            Just theUser -> do
                newId <- runSQL $ insert theUser
                json $ object ["result" .= String "success", "id" .= newId]
    get ("users" <//> var) $ \emporioId -> do
        ls <- runSQL $ selectList [UserStore ==. toSqlKey emporioId] [Asc UserId]
        json ls
    get ("users" <//> "delete" <//> var) $ \userId -> do
        ls <- runSQL $ deleteCascadeWhere [UserId ==. toSqlKey userId]
        json ()

    -- stores

    -- stores
    get ("stores" <//> var) $ \storeId -> do
        maybeStore <- runSQL $ P.get storeId :: ApiAction (Maybe Store)
        case maybeStore of
            Nothing -> jsonError 2 "Could not find a store with matchind id"
            Just theStore -> json theStore-}

