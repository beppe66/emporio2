{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module Mailing where

import           Data.Monoid
import           Data.String
import           Data.Text
import           Network.Mail.Client.Gmail
import           Network.Mail.Mime
import           System.Environment
import           Text.InterpolatedString.QM

compose :: (Monoid a , IsString a) => Text -> a
compose link = [qmb|
Usa il segente link per accedere al sistema empori

{link}

|]

mkAddress = Address Nothing

sendEmail :: Text -> Text -> IO ()
sendEmail to c = do
    sender <- fromString <$> getEnv "username"
    password <- fromString <$> getEnv "password"
    from <- mkAddress <$> fromString <$> getEnv "address"
    host <- fromString <$> getEnv "hosting"
    sendGmail sender password from [mkAddress to] [] []
        "Login sistema emporio"
        (compose $ host <> "/login/" <> c)
        []
        (10^10)
