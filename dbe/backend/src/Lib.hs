{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE TypeOperators              #-}

module Lib where

import           Control.Monad.Logger          (LoggingT, runStdoutLoggingT)
import           Control.Monad.Trans
import           Common.Data
import           Data.Aeson                    hiding (json)
import           Data.HVect
import           Data.Monoid                   ((<>))
import           Data.Text                     (Text, pack, unpack)
import           Database.Persist              hiding (get)
import qualified Database.Persist              as P
import           Database.Persist.Sqlite       hiding (get)
import           Database.Persist.TH
import           Network.Wai.Middleware.Static
import           System.Random
import           Web.Spock
import           Web.Spock.Config


type SessionVal = Maybe SessionId
type Api ctx = SpockCtxM ctx SqlBackend SessionVal () ()
type ApiAction ctx a = SpockActionCtx ctx SqlBackend SessionVal () a

runSQL
    :: (HasSpock m, SpockConn m ~ SqlBackend)
    => SqlPersistT (LoggingT IO) a
    -> m a
runSQL action = runQuery $ \conn -> runStdoutLoggingT $ runSqlConn action conn

jsonError :: Int -> Text -> ApiAction ctx xs
jsonError code message = json $ object
    [ "result" .= String "failure"
    , "error" .= object ["code" .= code, "message" .= message]
    ]

