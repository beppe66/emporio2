{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE ExistentialQuantification  #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE TypeOperators              #-}

module Roles where

import           Common.Communication
import           Control.Lens
import           Control.Monad
import           Control.Monad.Logger          (LoggingT, runStdoutLoggingT)
import           Control.Monad.Trans
import           Common.Data
import           Data.Aeson                    hiding (json)
import           Data.HVect                    hiding (pack)
import           Data.Maybe
import           Data.Monoid                   ((<>))
import           Data.Set                      (Set)
import qualified Data.Set                      as Set
import           Data.Text                     (Text, pack, unpack)
import qualified Database.Persist              as P
import           Database.Persist.Sqlite       hiding (get)
import           Database.Persist.TH
import           Lib
import           Network.Wai.Middleware.Static
import           System.Random
import           Web.Spock
import           Web.Spock.Config

storeOf RSuper        = Nothing
storeOf (RUser x)     = Just x
storeOf (ROperator x) = Just x
storeOf (RAdmin x)    = Just x

data BoxR
        = forall r
        . ( PersistEntityBackend r ~ SqlBackend
        , PersistEntity r
        ) => BoxR (Text -> Filter r) (r -> Role)

whoamiH
    :: ListContains n Authentication xs
    => ApiAction (HVect xs) (HVect (WhoAmI : xs))
whoamiH = do
    Authentication email _ <- findFirst <$> getContext
    cs                     <- foldM
        (expandSet email)
        mempty
        [ BoxR (AdminEmail ==.)             (RAdmin  . view adminStore)
        , BoxR (UserEmail ==. )             (RUser  . view userStore)
        , BoxR (OperatorEmail ==.)          (ROperator  . view operatorStore)
        , BoxR (SuperEmail ==.)             (const RSuper)
        ]
    ss <- runSQL $ selectList
        [Filter StoreId (Right $ catMaybes $ map storeOf (Set.toList cs)) In]
        []
    (WhoAmI email ss cs :&:) <$> getContext

expandSet
    :: ListContains n Authentication xs
    => Text
    -> Set Role
    -> BoxR
    -> ApiAction (HVect xs) (Set Role)
expandSet email s (BoxR h g) = do
    xs <- runSQL $ selectList [h email] []
    return $ s <> Set.fromList (map (g . entityVal) xs)
