module Common.Rational (module Data.Ratio, r1show, rtf) where

import           Data.Ratio
import           Data.Text

r1show = pack . show . (fromRational :: Rational -> Float)

rtf :: Float -> Rational
rtf = realToFrac
