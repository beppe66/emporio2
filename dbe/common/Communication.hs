{-# LANGUAGE TemplateHaskell #-}

module Common.Communication where


import           Common.Data
import           Data.Aeson.TH
import           Data.Char
import           Data.Set         (Set)
import           Data.Text        (Text)
import           Database.Persist (Entity)

data Role = RUser StoreId
          | ROperator StoreId
          | RAdmin StoreId
          | RSuper
          | RNoone deriving (Eq,Ord, Show)

$(deriveJSON
    defaultOptions{constructorTagModifier = map toLower}
        ''Role)

data WhoAmI = WhoAmI
            { whoamiEmail  :: Text
            , whoamiStores :: [Entity Store]
            , whoamiRoles  :: Set Role
            } deriving Eq

$(deriveJSON
    defaultOptions{constructorTagModifier = map toLower
                  , fieldLabelModifier = drop 4}
        ''WhoAmI)
