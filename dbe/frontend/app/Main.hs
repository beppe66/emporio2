{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE NoMonomorphismRestriction  #-}
{-# LANGUAGE OverloadedLists            #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE RecursiveDo                #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}


import           Control.Lens         ((^.))
import           Control.Monad
import           Data.Aeson           hiding (json)
import           Data.List
import           Data.Map             (Map)
import qualified Data.Map             as M
import           Data.Monoid          ((<>))
import           Data.Set             (Set)
import qualified Data.Set             as Set
import           Data.Text            (Text, pack, unpack)
import qualified Data.Text            as T
import           Data.Text.Encoding
import           Database.Persist     hiding (get)
import           Database.Persist
import           Database.Persist.Sql
import           Reflex
import           Reflex.Dom           hiding (Key, button, link, table)
import qualified Reflex.Dom

import           Admins
import           Cashing
import           Common.Communication
import           Common.Data
import           Lib
import           Operators
import           Products
import           Roles
import           Row
import           Stores
import           Users

data SuperView
        = ChangeStores
        | ChangeAdmins (Entity Store)

superView :: MS m => SuperView -> m (ES (Either () SuperView))
superView ChangeStores
    = fmap (Right . ChangeAdmins) <$> storesWidget
superView (ChangeAdmins s) = do
    ca <- adminsWidget s
    r <- divClass "subnav" $ (ChangeStores <$) <$> link "altri empori"
    return $ leftmost [Left <$> ca, Right <$> r]


data View
    = View WhoAmI Role
    | JumpView

viewerJ :: MS m => View -> m (ES (Either () View))
viewerJ JumpView = do
    divClass "subtitle" $ text "Impersonificazione"

    pb     <- getPostBuild
    authsE <- fmapMaybe id <$> getAndDecode ("jump" <$ pb)
    widgetHold spinner $ (<$> authsE) $ \(ts :: [Authentication]) -> table $ do
        -- el "thead" $ el "tr" $ el "th" $ text "autenticazioni"
        forM_ ts $ \(Authentication e c) ->
            el "tr" $ el "td" $ linkH ("login/" <> c) e
    return never

viewerJ (View w RSuper) = do
    rec
        se <- domMorph superView sd
        sd <- holdDyn ChangeStores (filterRight se)
    divClass "subtitle" $ text "Altre funzioni"
    r <- (JumpView <$) <$> link "Impersonificazione"
    return $ leftmost [Left <$> filterLeft se, Right <$> r]

viewerJ (View w (RAdmin s))
    = do

        vE <- elClass "ul" "admin_view_select" $ do
                p <- el "li" $ button $ "Prodotti"
                o <- el "li" $ button $ "Cassieri"
                u <- el "li" $ button $ "Utenti"
                return $ leftmost [ProductsV <$ p, OperatorsV <$ o, UsersV <$ u]
        vD <- holdDyn ProductsV vE
        divClass "admin_view" $ do
            a <- flip domMorph vD  $ \case
                ProductsV -> productsWidget s 10 >> return never
                OperatorsV -> operatorsWidget s
                UsersV ->  usersWidget s 10
            return $  Left () <$ a

viewerJ (View w (ROperator s)) = do
    cashingW s >> return never
viewerJ (View w (RUser s)) = return never

viewer Nothing  = return never
viewer (Just v) = viewerJ v

data AView = ProductsV | OperatorsV| UsersV
-- selectRoleW rs = fmap leftmost $ table $ forM rs $ \

mainView :: forall m . MS m => m ()
mainView = do
    pb <- getPostBuild
    rec
        whoE :: ES (Maybe WhoAmI) <- getAndDecode ("whoami" <$ leftmost [pb, c])
        whoD :: DS (Maybe WhoAmI) <- holdDyn Nothing $ snd <$> ffilter fst change
        let f :: MS m => Maybe WhoAmI -> m (ES ())
            f Nothing = spinner >> return never
            f (Just w) = do
                let roles = M.fromList $ zip [1::Int ..] $ renderRoles w
                pb <- divClass "whoami" $ button $ whoamiEmail w
                let rsW = divClass "role_select" $ fmap leftmost
                            $ table
                            $ forM (M.assocs roles)
                            $ \(_,(r,t))
                                -> el "tr" $ el "td" $ (r <$) <$> button t
                r <- modalS $ (flip (<$) pb $ ("Ruolo ...", rsW))
                rec
                    cvE <- domMorph viewer cvD
                    cvD <- holdDyn Nothing
                            $ leftmost
                            [  Just <$> filterRight cvE
                            ,   fmap (View w) <$> r
                            ]
                return $ filterLeft cvE
            change = attachWith isChanged (current whoD) whoE
        c <- domMorph f whoD
    return ()


main :: IO ()
main = mainWidget $ do
    css
    awesome
    viewport
    favicon
    mainView
    divClass "bottom" $ return ()
css =
    elAttr
            "link"
            [("rel", "stylesheet"), ("type", "text/css"), ("href", "amministrazione.css")]
        $ return ()

awesome =
    elAttr "script"
        [("src", "https://use.fontawesome.com/0b08d58b80.js")
        , ("type", "text/javascript")]
        $ return ()

viewport = elAttr "meta"
    [("name","viewport"),("content","width=device-width, user-scalable=no,initial-scale=1.0")]
    $ return ()

favicon = elAttr "link"
        [("rel","shortcut icon"),("type","image/png"),("href","favicon.png")]
        $ return ()
