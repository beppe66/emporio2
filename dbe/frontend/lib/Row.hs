{-# LANGUAGE ConstraintKinds           #-}
{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE InstanceSigs              #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE OverloadedLists           #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE Rank2Types                #-}
{-# LANGUAGE RecursiveDo               #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TemplateHaskell           #-}
{-# LANGUAGE TypeFamilies              #-}

module Row where

import           Control.Category
import           Control.Concurrent
import           Control.Lens           (makeLenses, set, view, (&), (.~), (^.))
import           Control.Monad          (forM, forM_, void)
import           Control.Monad
import           Control.Monad.Identity (Identity)
import           Control.Monad.Identity
import           Control.Monad.Reader
import           Control.Monad.Trans
import           Control.Monad.Trans
import           Data.Aeson
import           Data.Dependent.Map     (DMap, DSum ((:=>)), singleton)
import qualified Data.Dependent.Map     as DMap
import           Data.Either
import           Data.GADT.Compare
import           Data.GADT.Compare      (GCompare)
import           Data.IORef
import qualified Data.Map               as M
import           Data.Maybe
import           Data.Semigroup
import           Data.Text              (Text, pack)
import           Data.Text.Encoding
import           Data.Time.Clock
import           Data.Time.Clock
import           Database.Persist
import           Database.Persist.Sql
import           GHC.Exts
import qualified GHCJS.DOM.HTMLElement  as J
import           Lib
import           Prelude                hiding (id, lookup, (.))
import           Reflex                 hiding (Key)
import qualified Reflex                 as Reflex
import           Reflex.Dom             hiding (Key, button)
import           System.Random



data Cell m a = Cell
    { _cellValue   :: Either Text Text
    , cellEmpty    :: m (ES ())
    , cellFull     :: Text -> m (ES ())
    , cellValidate :: Text -> Maybe a
    }

makeLenses ''Cell

data DoneEdit a
    = DoneEdit
        { _changeText  :: Text
        , _changeValue :: a
        , _changeTab   :: Bool
        }

cell :: MS m => Cell m a -> Bool -> m (ES (Either Bool (DoneEdit a)))
cell (Cell x0 ce cf fv) True = do
    rec
        t <- textInput
            $ def
            & either (\x -> textInputConfig_attributes
                .~ constDyn [("placeholder", x)]
                )
                (\x -> textInputConfig_initialValue .~ x)
                x0
        v <- holdDyn Nothing $ (\t -> (,) t <$> fv t) <$> t ^. textInput_input
        let enter = keypress Enter t
            abort = domEvent Blur t
            -- next = keypress Tab t
            z = leftmost
                [   fmap (\(t,v) -> Right (DoneEdit t v True)) $ fmapMaybe id
                        $ tag (current v)
                        $ leftmost [enter, abort]
                ,   Left False <$ leftmost [enter, abort]
                ]
    getPostBuild >>= \pb -> setFocus (t <$ pb)
    return z
cell (Cell x0 ce cf fv) False
    = fmap (Left True <$)
    $ either
        (\_ -> ce)
        cf
        x0


cellW :: MS m => ES Bool -> Cell m a -> m (ES (a, Bool))
cellW ch c0 = do
    rec xD <- holdDyn c0 $ (\(DoneEdit t _ _)
                           -> set cellValue (Right t) c0) <$> filterRight cE
        cE <- domMorph (uncurry cell) $ (,) <$> xD <*> cD
        -- let cE = never
        cD <- holdDyn False $ leftmost [False <$ filterRight cE, filterLeft cE, traceEventWith show ch]
    return $ (\(DoneEdit _ v b) -> (v,b)) <$> filterRight cE

data NewRow m a = NewRow
            { newRowIndependent :: [(Text, Text -> Bool)]
            , newRowComplete    :: [Text] -> Maybe a
            , newRowCantSubmit  :: m ()
            , newRowSubmit      :: m (ES ())
            }

newRow :: forall m a . MS m => m (ES ()) -> (Text -> m (ES ()))  -> NewRow m a -> m (ES a)

newRow i f  (NewRow ts' t csm ck) = do
    let ts = M.fromList $ zip [0..]
            [ Cell (Left t) i  f (\z -> if c z then Just z else Nothing)
            | (t, c) <- ts'
            ]
    rec
        mts :: DS (M.Map Int (Cell m Text)) <- holdDyn ts tsE
        es <- listViewWithKey mts
            $ \i dc -> domMorph (cellW (True <$ ffilter ((==) i) ch )) dc
        let g :: M.Map Int (Cell m Text) -> M.Map Int (Text,Bool) -> M.Map Int (Cell m Text)
            g ts
                = flip M.union ts
                . M.mapWithKey (\k (v,_) -> cellValue .~ (Right v) $ ts M.! k)
            tsE :: ES (M.Map Int (Cell m Text))
            tsE = attachWith g (current mts) es
            ch = fmapMaybe (\(i, (_,c)) -> if c then Just (i + 1) else Nothing) $ head . M.assocs <$> es
    let good (Cell (Right t) _ _ _) = Just t
        good _                      = Nothing
        r = flip fmap mts $ \m -> do
                    ts <- sequence . map good $ M.elems m
                    t ts
        add Nothing  = csm >> return never
        add (Just m) = (m <$) <$> ck
    domMorph add r

rowW    :: forall m a . MS m
        =>  m (ES ())
        -> (Text  -> m (ES ()))
        -> [(Text, Text -> Maybe a)]
        -> m (ES a)
rowW i f xs = cellsW  [Cell (Right v) i f g | (v, g) <- xs]
    where
    cellsW :: MS m => [Cell m a] -> m (ES a)
    cellsW cs = fmap (fmap fst . leftmost)  $ forM cs $  cellW never

renderKey k = pack (show $ fromSqlKey k)

data FieldUpdate = forall b . ToJSON b => FU Text b
rowUpdateNet  :: (MS m, FromJSON a, ToBackendKey SqlBackend a)
        => Entity a
        -> Text -- endpoint
        -> (Maybe a -> m (ES FieldUpdate))
        -> m (ES (Key a))

rowUpdateNet (Entity k x) ep f = do
    rec
        e <- domMorph f d
        e' <- performRequestAsync $
                (\(FU x v) -> postJson
                    (ep
                    <> "/"
                    <> pack (show $ fromSqlKey k)
                    <> "/"
                    <> x
                    )
                    v
                )
                <$> e
        e'' <- getAndDecode $ (ep <> "/" <> pack (show $ fromSqlKey k)) <$ e'
        d <- holdDyn (Just x) e''
    return (k <$ e'')

rowNewNet
    :: forall a m
    . (MS m, FromJSON (Key a), FromJSON a, ToJSON a, ToBackendKey SqlBackend a)
    => Text -- endpoint post
    -> ES a
    -> m (ES a)
rowNewNet ep e = do
    (ek :: ES (Key a)) <-
        fmap (fmapMaybe id . fmap decodeXhrResponse)
        .   performRequestAsync
        $   postJson ep
        <$> e
    fmap (fmapMaybe id)
        $   getAndDecode
        $   (\k -> ep <> "/" <> pack (show $ fromSqlKey k))
        <$> ek
newRowTdp =newRow (el "td" $ icon ["plus"]) $ \t ->
        domEvent Click <$> fst <$> elAttr' "td" [] (text t)

rowWTdp = rowW (el "td" $ icon ["plus"]) $ \t ->
        domEvent Click <$> fst <$> elAttr' "td" [] (text t)

scrolling w g ps = do
    rec
        z <- foldDyn ($) 0 $ f <$> filterLeft s
        let     f True x
                        | x + w >= length ps = x
                        | otherwise = x + w
                f False x = max 0 (x - w)
                r x = take w . drop x $ ps
        s <- domMorph g $ r <$> z
    return $ filterRight s
