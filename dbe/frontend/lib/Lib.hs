{-# LANGUAGE ConstraintKinds           #-}
{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE InstanceSigs              #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE Rank2Types                #-}
{-# LANGUAGE RecursiveDo               #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeFamilies              #-}

{-# LANGUAGE OverloadedLists           #-}
{-# LANGUAGE OverloadedStrings         #-}
module Lib where

import           Control.Category
import           Control.Concurrent
import           Control.Lens                (view, (&), (.~))
import           Control.Monad               (forM, forM_, void)
import           Control.Monad
import           Control.Monad.Identity      (Identity)
import           Control.Monad.Identity
import           Control.Monad.Reader
import           Control.Monad.Trans
import           Control.Monad.Trans
import           Data.Dependent.Map          hiding (delete)
import           Data.Dependent.Map          (DMap, DSum ((:=>)), singleton)
import qualified Data.Dependent.Map          as DMap
import           Data.Either
import           Data.GADT.Compare
import           Data.GADT.Compare           (GCompare)
import           Data.IORef
import qualified Data.Map                    as M
import           Data.Semigroup
import           Data.Text                   (Text, pack, unpack)
import           Data.Time.Clock
import           Data.Time.Clock
import           GHC.Exts
import qualified GHCJS.DOM                   as J
import qualified GHCJS.DOM.HTMLElement       as J
import qualified GHCJS.DOM.Window            as JW
import           Language.Javascript.JSaddle (MonadJSM, liftJSM)
import           Prelude                     hiding (id, lookup, (.))
import           Reflex                      hiding (combineDyn)
import qualified Reflex                      as Reflex
import           Reflex.Dom                  hiding (button, combineDyn)
import           System.Random

-- | delay an Event by the amount of time specified in its value
step     :: MonadWidget t m
                => Event t (NominalDiffTime,a) -- ^ delay time in seconds +
                -> m (Event t a)
step e
    =  performEventAsync
    . ffor e
    $ \(dt,a) cb -> liftIO
                        . void
                        . forkIO
                        . forever $ do
                            threadDelay . ceiling $ dt * 1000000
                            cb a

repeating :: MS m => NominalDiffTime -> ES a -> m (ES a)
repeating m e = do
    rec l <- step $ (,) m <$> e
    return $ leftmost [e,l]
-------  reflex missings --------------
type Morph t m a = Dynamic t (m a) -> m (Event t a)

mapMorph  :: (MonadHold t m, Reflex t) => Morph t m (Event t b) -> (a -> m (Event t b)) -> Dynamic t a -> m (Event t b)
mapMorph dyn f d = dyn (f <$> d) >>= joinE

joinE :: (Reflex t, MonadHold t f) => Event t (Event t a) -> f (Event t a)
joinE = fmap switch . hold never

pick :: (GCompare k, Reflex t) => k a -> Event t (DMap k Identity) -> Event t a
pick x r = select (fan r) x -- shouldn't we share fan r ?

gateWith f = attachWithMaybe $ \allow a -> if f allow then Just a else Nothing

pair x = leftmost . (: [x])

sselect = flip select
------------- Spider synonims

type ES = Event Spider
type DS = Dynamic Spider
type BS = Behavior Spider

-------------- Dom + spider synonyms

type MS = MonadWidget Spider
type Message a = DMap a Identity
type Cable a = ES (Message a)

-- specialized mapMorph for the Dom host
domMorph ::     MonadWidget t m
                => (a -> m (Event t b))  -- widget rewriter
                -> Dynamic t a           -- driver for rewritings
                -> m (Event t b)         -- signal the happened rewriting
domMorph = mapMorph dyn

-------------- create a Cable ----------
mergeDSums :: GCompare a => [DSum a ES] -> Cable a
mergeDSums = merge . DMap.fromList

wire' :: (GCompare k) => k v -> ES v -> Cable k
wire' x = merge . singleton x

wire (k :=> v) = wire' k v
------------- Lib -------------------------------------

-- | delay an Event by the amount of time specified in its value and random
-- pick from chances
--



-- polymorphic 2-keys type DSum
data EitherG r l a where
  RightG :: EitherG l r r
  LeftG ::  EitherG l r l

instance GEq (EitherG r l) where
  RightG `geq` RightG = Just Refl
  LeftG `geq` LeftG = Just Refl
  _ `geq` _ = Nothing

instance GCompare (EitherG r l) where
  RightG `gcompare` LeftG = GLT
  LeftG `gcompare` RightG = GGT
  RightG `gcompare` RightG = GEQ
  LeftG `gcompare` LeftG = GEQ

-- onEithersG :: (r -> r') -> (l -> l') -> DSum (EitherG l r) f -> DSum (EitherG l' r') f
onEithersG :: (r -> r') -> (l -> l') -> (forall f. Functor f => DSum (EitherG l r) f -> DSum (EitherG l' r') f)
onEithersG fr fl (RightG :=> x) = RightG :=> ( fr <$>  x)
onEithersG fr fl (LeftG :=> x)  = LeftG :=> (fl <$> x)
-- eithersG :: (r -> r') -> (l -> l') -> DSum (EitherG l r) ES -> DSum (EitherG l' r') ES
-- eithersG = onEithersG
rightG b = wire (RightG :=> b)
leftG b = wire (LeftG :=> b)



instance GCompare k => IsList (DMap k f) where
  type Item (DMap k f) = DSum k f
  fromList = Data.Dependent.Map.fromList
  toList = Data.Dependent.Map.toList

-------------------------------------------
righting e = (\(Right x) -> x) <$> ffilter isRight e
lefting e = (\(Left x) -> x) <$> ffilter isLeft e
----------------------------------------
icon :: MS m => [Text] -> m (ES ())
icon xs = do
    (r,_) <- divClass "icon"
        $ elAttr' "i" [("class",foldl (\y x -> y <> " fa-" <> x ) "fa" xs)]
        $ return ()
    return $ domEvent Click r

{-
class HasInput m a where
  getInput :: m (DS (Maybe a))

class HasIcons m a where
  getIcon :: a -> m (ES ())

icon :: (MS m, MonadReader (DS r) m, In Bool r) => [Text] -> Text -> m (ES ())
icon xs t =divClass "icon-box" $  do
                (r,_) <- divClass "icon" $ elAttr' "i" [("class",foldl (\y x -> y <> " fa-" <> x ) "fa" xs)] $ return ()
                asks (fmap see)  >>= domMorph (\q -> if q then divClass "hints" (text t) >> return never else return never )
                return $ domEvent Click r

composeIcon :: (MonadReader (DS r) m, In Bool r, MS m) => m (ES a) -> m (ES a)  -> m (ES a)
composeIcon x y = fmap leftmost . divClass "compose" . sequence $ [
  divClass "compose-x" x,
  divClass "compose-plus" (icon ["plus","2x"] "or") >> return never,
  divClass "compose-y" y
  ]


data Iconified = Iconified | Disclosed
floater x = do
  t <- asks (fmap see)
  elDynAttr "div" ((\t -> M.singleton "class" (if t then "combo-containerHints" else "combo-container")) <$> t ) . divClass "combo-buttons" $ x



check c f = if c then f else return never
-}

-- pure

button :: MonadWidget t m => Text -> m (Event t ())
button s = do
    (e, _) <-
        elAttr'
                "button"
                [("class", "pure-button")]
            $ text s
    return $ domEvent Click e

link :: MonadWidget t m => Text -> m (Event t ())
link s = do
    (e, _) <-
        elAttr'
                "a"
                [("class", "pure-menu-button"), ("href", "#")]
            $ text s
    return $ domEvent Click e

linkH :: MonadWidget t m => Text -> Text -> m ()
linkH h s = do
    (e, _) <-
        elAttr'
                "a"
                [("class", "pure-menu-link"), ("href", h)]
            $ text s
    return ()
table = elClass "table" "pure-table pure-table-bordered"
form = elClass "form" "pure-form"
menu = divClass "pure-menu"
ul = elClass "ul" "pure-menu-list"
li = elClass "li" "pure-menu-item"

spinnerE :: MS m => m (ES ())
spinnerE = do
    icon ["spinner", "spin"]

spinner :: MS m => m ()
spinner = spinnerE >> return ()

setFocus :: MS m => ES (TextInput Spider) -> m ()
setFocus e = performEvent_ . ffor e $ \t -> do
    liftIO $ forkIO $ do
        threadDelay $ 10^5
        J.focus . _textInput_element $ t
    return ()

modalM :: MS m => ES (Text, m ()) -> m (ES ())
modalM d =  do
    let     mod Nothing = return never
            mod (Just (h,f)) = divClass "modal_out" $ divClass "modal" $ do
                divClass "modal-reason" $ do
                    divClass "modal-reason-title" $ do
                        text h
                    divClass "modal-selection" $ do
                        f
                divClass "modal-choice" $ do
                    n <- divClass "modal-no" $ icon ["times"]
                    y <- divClass "modal-yes" $ icon ["check"]
                    return $ leftmost [True <$ y, False <$ n]
    rec modE <- domMorph mod modD
        modD <- holdDyn Nothing
                    $ leftmost
                    [ Just <$> d
                    , Nothing <$ modE
                    ]
    return $ () <$ ffilter id modE

modal :: MS m => ES Text -> m (ES ())
modal d = modalM $ flip (,) (return ()) <$> d


modalS :: MS m => ES (Text, m (ES a)) -> m (ES (Maybe a))
modalS d = divClass "modal_out" $ do
    let     mod Nothing = return never
            mod (Just (h,f)) = divClass "modal" $ do
                e1 <- divClass "modal-reason" $ do
                    divClass "modal-reason-title" $ do
                        text h
                    divClass "modal-selection" $ do
                        f
                e2 <- divClass "modal-choice" $ do
                    divClass "modal-no" $ icon ["times"]
                return $ leftmost [Just <$> e1,Nothing <$ e2]
    rec modE <- domMorph mod modD
        modD <- holdDyn Nothing
                    $ leftmost
                    [ Just <$> d
                    , Nothing <$ modE
                    ]
    return modE


isChanged :: Eq a => Maybe a -> Maybe a -> (Bool, Maybe a)
isChanged (Just x) (Just y)
    | x /= y = (True, Just y)
    | otherwise = (False, Just y)
isChanged _ y = (True, y)
--  library -------------
minzero x y | x + y >= 0 = x + y
            | True = 0

ite x _ True = x
ite _ y _    = y
---------------------

-- | sample internal width
iw
    :: (PerformEvent t m, MonadJSM (Performable m))
    => Event t b
    -> m (Event t Int)
iw x = performEvent ((liftJSM J.currentWindowUnchecked >>= JW.getInnerWidth) <$ x)

pshow = pack . show
valid x = case reads . unpack $ x of
                [(v,"")] -> Just v
                _        -> Nothing

coupleWith :: (MonadFix m, MonadHold t m, Reflex t)
    => (Event t a -> m (Event t b))
    -> Event t a
    -> m (Event t (a,b))
coupleWith f aE = do
    rec
        let  gE = gate (current w) aE
        w <- holdDyn True $  leftmost [False <$ gE,True <$ bE]
        bE <- f gE
    gD <- holdDyn undefined gE
    return $ attachPromptlyDyn gD bE
