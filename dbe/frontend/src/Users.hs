{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE NoMonomorphismRestriction  #-}
{-# LANGUAGE OverloadedLists            #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE RecursiveDo                #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

module Users where

import           Data.Aeson           hiding (json)
import           Data.Monoid          ((<>))
import           Data.Text            (Text, pack, unpack)
import qualified Data.Text            as T
import           Data.Text.Encoding

import           Common.Communication
import           Control.Lens         ((^.))
import           Control.Monad
import           Common.Data
import           Data.List
import           Data.Map             (Map)
import qualified Data.Map             as M
import           Data.Maybe
import           Data.Set             (Set)
import qualified Data.Set             as Set
import           Database.Persist     hiding (get)
import           Database.Persist
import           Database.Persist.Sql
import           Lib
import           Common.Rational
import           Reflex
import           Reflex.Dom           hiding (Key, button, link, table)
import qualified Reflex.Dom
import           Roles
import           Row

-- updateRow :: MS m => Maybe Product -> m (ES )
updateRow (Just pr@(User t e n p _ s)) = rowWTdp
        [ (pshow t, fmap (FU "card")  . (valid :: Text -> Maybe Int))
        , (e, \x -> if "@" `T.isInfixOf` x
                    then Just $ FU "email" x
                    else Nothing
          )
        , (n, \x -> Just (FU "nickname" x))
        , (r1show p, fmap (FU "points")  . fmap (realToFrac :: Float -> Rational) . valid)
        , (s, \case
                    "A" -> Just (FU "state" ("A" :: Text))
                    "P" ->  Just (FU "state" ("P" :: Text))
                    _ -> Nothing
          )
        ]
updateRow Nothing = spinner >> return never

makeRow :: MS m => Key Store -> m (ES User)
makeRow k
    = newRowTdp
    $ NewRow
        [ ("tessera utente", (isJust :: Maybe Int -> Bool) . valid)
        , ("email utente", \x -> "@" `T.isInfixOf` x)
        , ("nick name", const True)
        , ("punteggio mensile", (isJust :: Maybe Float -> Bool) . valid)
        , ("stato", (`T.isInfixOf` "AP"))
        ]
        (\[t,e,n,p,v] -> User <$> valid t <*> pure e <*> pure n
                    <*> (rtf <$> valid p) <*> pure k <*> pure v)
        (el "td" $ icon ["ban"] >> return ())
        $ el "td" $ icon ["check"]

users :: MS m => Key Store -> [Entity User] -> m (ES (Either Bool ()))
users k us = do
    control <- divClass "subtitle" $ do
        text "Anagrafe Utenti"
        elClass "ul" "scroll" $ do
                h <- el "li" $ icon ["arrow-down"]
                t <- el "li" $ icon ["arrow-up"]
                return $ leftmost [True <$ h, False <$ t]
    r <- divClass "users" $ table $ do
        el "thead" $ el "tr" $ do
            mapM_ (el "th" . text)
                (["tessera", "email", "nome","punteggio","stato", "-"] :: [Text])
        d <- fmap leftmost $ forM us $ \y@(Entity rk x) ->
            el "tr" $ do
                rowUpdateNet y ("user/" <> renderKey k) updateRow
                d <- elClass "td" "delete" $ icon ["trash"]
                dd <- modal $ ("elimina utente " <> x ^. userNickname <> " ?") <$ d
                ddd <- modal $ ("elimina veramente utente " <> x ^. userNickname <> " ?") <$ dd
                fmap (fmap (\(_ :: Maybe ()) -> ()))
                    $  getAndDecode
                    $  ("users/" <> renderKey k <> "/delete/" <> renderKey rk)
                    <$ ddd
        a <- fmap (() <$) $ el "tr" $
            makeRow k >>= rowNewNet ("user/" <> renderKey k)
        return $ leftmost [a,d]
    return $ leftmost [Right <$> r, Left <$> control]

usersWidget :: MS m => Key Store -> Int -> m (ES ())
usersWidget k w = do
    let ep = "users/" <> renderKey k
    pb <-  getPostBuild
    rec
        ms <- getAndDecode $ ep <$ leftmost [pb, s]
        ps :: DS (Maybe [Entity User]) <- holdDyn Nothing $ ms
        let v Nothing   = spinnerE
            v (Just us) = scrolling w (users k) us
        s <- domMorph v ps
    return s

