{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE NoMonomorphismRestriction  #-}
{-# LANGUAGE OverloadedLists            #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE RecursiveDo                #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

module Products where

import           Data.Aeson           hiding (json)
import           Data.Monoid          ((<>))
import           Data.Text            (Text, pack, unpack)
import qualified Data.Text            as T
import           Data.Text.Encoding

import           Common.Communication
import           Control.Lens         (view, (^.))
import           Control.Monad
import           Common.Data
import           Data.List
import           Data.List
import           Data.Map             (Map)
import qualified Data.Map             as M
import           Data.Maybe
import           Data.Set             (Set)
import qualified Data.Set             as Set
import           Database.Persist     hiding (get)
import           Database.Persist
import           Database.Persist.Sql
import           Lib
import           Common.Rational
import           Reflex
import           Reflex.Dom           hiding (Key, button, link, table)
import qualified Reflex.Dom
import           Roles
import           Row

-- updateRow :: MS m => Maybe Product -> m (ES )
updateRow (Just pr@(Product n p i _ s)) = rowWTdp
        [ (n, \x -> Just (FU "name" x))
        , (r1show p, fmap (FU "points")  . fmap (realToFrac  :: Float -> Rational) . valid)
        , (i, \x -> Just (FU "image" x))
        , (s, \case
                    "A" -> Just (FU "state" ("A" :: Text))
                    "P" ->  Just (FU "state" ("P" :: Text))
                    _ -> Nothing
          )
        ]
updateRow Nothing = spinner >> return never

makeRow :: MS m => Key Store -> m (ES Product)
makeRow k
    = newRowTdp
    $ NewRow
        [ ( "nome prodotto", const True)
        , ( "prezzo prodotto", (isJust :: Maybe Float -> Bool)  . valid)
        , ( "url immagine", const True)
        , ( "stato", (`T.isInfixOf` "AP"))
        ]
        (\[n,p,i,s] -> Product <$> pure n <*> (rtf <$> valid p) <*> pure i <*> pure k <*> pure s)
        (el "td" $ icon ["ban"] >> return ())
        $ el "td" $ icon ["check"]

products :: MS m => Key Store -> [Entity Product] -> m (ES (Either Bool ()))
products k us = do
    control <- divClass "subtitle" $ do
        text "Anagrafe Prodotti"
        elClass "ul" "scroll" $ do
                h <- el "li" $ icon ["arrow-down"]
                t <- el "li" $ icon ["arrow-up"]
                return $ leftmost [True <$ h, False <$ t]
    r <- divClass "products" $ table $ do
        el "thead" $ el "tr" $ do
            mapM_ (el "th" . text)
                (["nome", "prezzo","immagine","stato","-"] :: [Text])
        d <- fmap leftmost $ forM us $ \y@(Entity rk x) ->
            el "tr" $ do
                rowUpdateNet y ("product/" <> renderKey k) updateRow
                d <- elClass "td" "delete" $ icon ["trash"]
                dd <- modal $ ("elimina prodotto ?") <$ d
                fmap (fmap (\(_ :: Maybe ()) -> ()))
                    $  getAndDecode
                    $  ("products/" <> renderKey k <> "/delete/" <> renderKey rk)
                    <$ dd
        a <- fmap (() <$) $ el "tr" $
            makeRow k >>= rowNewNet ("product/" <> renderKey k)
        return $ leftmost [a,d]
    return $ leftmost [Right <$> r, Left <$> control]

productsWidget :: MS m => Key Store -> Int -> m ()
productsWidget k w = do
    let ep = "products/" <> renderKey k
    pb <-  getPostBuild
    rec
        ms <- getAndDecode $ ep <$ leftmost [pb, s]
        ps :: DS (Maybe [Entity Product]) <- holdDyn Nothing $ ms
        let v Nothing = icon ["spinner", "spin"] >> return never
            v (Just rs) = scrolling w (products k)
                    (sortOn (view productName . entityVal) rs)
        s <- domMorph v ps
    return ()


