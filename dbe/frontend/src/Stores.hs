{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE NoMonomorphismRestriction  #-}
{-# LANGUAGE OverloadedLists            #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE RecursiveDo                #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

module Stores where

import           Data.Aeson           hiding (json)
import           Data.Monoid          ((<>))
import           Data.Text            (Text, pack, unpack)
import qualified Data.Text            as T
import           Data.Text.Encoding

import           Common.Communication
import           Control.Lens         ((^.))
import           Control.Monad
import           Common.Data
import           Data.List
import           Data.Map             (Map)
import qualified Data.Map             as M
import           Data.Set             (Set)
import qualified Data.Set             as Set
import           Database.Persist     hiding (get)
import           Database.Persist
import           Database.Persist.Sql
import           Lib
import           Reflex
import           Reflex.Dom           hiding (Key, button, link, table)
import qualified Reflex.Dom
import           Roles
import           Row

updateRow :: (MS m) => Maybe Store -> m (ES FieldUpdate)
updateRow (Just (Store n)) = rowWTdp [(n, \x -> Just (FU "name" x))]

updateRow Nothing          = spinner >> return never

stores :: MS m => Maybe [Entity Store] -> m (ES (Either (Entity Store) ()))
stores Nothing   = icon ["spinner", "spin"] >> return never
stores (Just us) = do
    divClass "subtitle" $ text "Anagrafe empori"
    table $ do
        s <- fmap leftmost $ forM us $ \x@(Entity k (Store n)) ->
            el "tr" $ do
                rowUpdateNet x "store" updateRow
                dE <- elClass "td" "delete" $ do
                    d <-  icon ["trash"]
                    dd <- modal $ ("elimina emporio " <> n <> " ?") <$ d
                    ddd <- modal $ ("veramente elimina emporio " <> n <> " ?") <$ dd
                    fmap (fmap (\(_ :: Maybe ()) -> ()))
                        $  getAndDecode
                        $  ("stores/delete/" <> renderKey k)
                        <$ ddd
                sE <- elClass "td" "select" $ (x <$) <$> icon ["arrow-right"]
                return $ leftmost [Right <$> dE, Left <$> sE]
        a <- fmap (Right () <$) $ el "tr" $ do
            a <- newRowTdp
               $ NewRow
                    [("nome emporio", const True)]
                    (\[t] -> Just $ Store t)
                    (el "td" $ icon ["ban"] >> return ())
                    $ el "td" $ icon["check"]
            el "td" $ icon ["circle"]
            rowNewNet "store"  a
        return $ leftmost [a, s]

storesWidget :: MS m => m (ES (Entity Store))
storesWidget = do
    pb <-  getPostBuild
    rec
        ms <- getAndDecode $ "stores" <$ leftmost [pb, filterRight s]
        ps :: DS (Maybe [Entity Store]) <- holdDyn Nothing $ ms
        s <- domMorph stores ps
    return $ filterLeft s

