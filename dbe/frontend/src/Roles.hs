{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE NoMonomorphismRestriction  #-}
{-# LANGUAGE OverloadedLists            #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE RecursiveDo                #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

module Roles where

import           Data.Aeson           hiding (json)
import           Data.Monoid          ((<>))
import           Data.Text            (Text, pack, unpack)
import           Data.Text.Encoding

import           Common.Communication
import           Control.Arrow        ((&&&))
import           Control.Lens         (view)
import           Control.Monad
import           Common.Data
import           Data.List
import           Data.Set             (Set)
import qualified Data.Set             as Set
import           Database.Persist     hiding (get)
import           Database.Persist
import           Database.Persist.Sql
import           Lib
import           Reflex
import           Reflex.Dom           hiding (Key, button, link)


resolveStoreName :: WhoAmI -> StoreId -> Text
resolveStoreName (WhoAmI _ cs _) t = case find ((==) t . entityKey) cs of
    Nothing -> error "broken whoami"
    Just x  -> view storeName . entityVal $ x

selectRole :: MS m => WhoAmI -> m (ES Role)
selectRole w@(WhoAmI _ _ xs) = fmap leftmost $ do
    let res = resolveStoreName w
    menu $ ul $ forM (Set.elems xs) $ \r -> do
        fmap (r <$) $ li $ case r of
            RSuper      -> link "amministratore di sistema"
            RAdmin    s -> link $ "amministratore dell'emporio di " <> res s
            ROperator s -> link $ "cassiere dell'emporio di " <> res s
            RUser     s -> link $ "utente dell'emporio di " <> res s

renderRoles w@(WhoAmI _ _ xs) =
    let res = resolveStoreName w
        f x = case x of
            RSuper      -> "amministratore di sistema"
            RAdmin    s -> "amministratore dell'emporio di " <> res s
            ROperator s -> "cassiere dell'emporio di " <> res s
            RUser     s -> "utente dell'emporio di " <> res s
    in map (id &&& f) $ Set.elems xs
