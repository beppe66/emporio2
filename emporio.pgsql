create extension pgcrypto;
CREATE TABLE "public"."empori" (
    "emporio" bigserial NOT NULL,
    "nominativo" text NOT NULL,
    CONSTRAINT "empori_nominativo_key" UNIQUE ("nominativo"),
    CONSTRAINT "empori_pkey" PRIMARY KEY ("emporio")
) WITH (oids = false);


CREATE TABLE "public"."amministratori" (
    "amministratore" bigserial NOT NULL,
    "emporio" integer,
    "email" text NOT NULL,
    "password" text,
    CONSTRAINT "amministratori_email_key" UNIQUE ("email"),
    CONSTRAINT "amministratori_pkey" PRIMARY KEY ("amministratore"),
    CONSTRAINT "amministratori_emporio_fkey" FOREIGN KEY (emporio) REFERENCES empori(emporio) NOT DEFERRABLE
) WITH (oids = false);


CREATE TABLE "public"."utenti" (
    "utente" bigserial NOT NULL,
    "emporio" integer NOT NULL,
    "nominativo" text NOT NULL,
    "telefono" text,
    "residuo" real,
    "punti" real,
    CONSTRAINT "utenti_nominativo_key" UNIQUE ("nominativo"),
    CONSTRAINT "utenti_pkey" PRIMARY KEY ("utente"),
    CONSTRAINT "utenti_emporio_fkey" FOREIGN KEY (emporio) REFERENCES empori(emporio) NOT DEFERRABLE
) WITH (oids = false);

CREATE FUNCTION reset_punti() RETURNS void AS $$
    update utenti 
        set residuo = punti;
    $$ LANGUAGE SQL;

create function nuovo_amministratore(emporio integer, email text, password text) returns void as $$
       INSERT INTO amministratori (emporio, email, password) VALUES 
	(emporio, email,  crypt('password1', gen_salt('bf', 8)));
        $$ LANGUAGE SQL;

INSERT INTO "empori" ("nominativo") VALUES ('borgotaro');
select nuovo_amministratore(1,'paolo.veronelli@gmail.com', 'enterogermina');
